
var IMCGlobal = 0;
var EdadGlobal = 0;
var PesoGlobal = 0;
var NivelGlobal;
var AlturaGlobal=0;

function generar(){
  let Edad = document.getElementById('Edad');
  let Altura = document.getElementById('Altura');
  let Peso = document.getElementById('Peso');

  Edad.value=Math.floor(Math.random()*(100-18)+18);
  Altura.value=(Math.random()*(2.5-1.5)+1.5).toFixed(2);
  Peso.value=(Math.random()*(130-20)+20).toFixed(2);

}
function calcular(){
   var Edad, Peso,Altura,IMC,Nivel;
   Peso=document.getElementById("Peso").value;
   Altura=document.getElementById("Altura").value/100;
   IMC=Peso/(Altura*Altura);
   document.getElementById("IMC").value=IMC.toFixed(2);
         
    if (IMC < 18.5)
    {
        
      Nivel = "peso bajo.";

    }
    else if (IMC >= 18.5 && IMC < 24.9)
    {
        
      Nivel = "Peso saludable.";
    }
    else if (IMC >= 24.9 && IMC < 29.9)
    {
        
      Nivel = "Sobrepeso.";
    }
    else
    {
        
      Nivel = "Obesidad.";
    } 
    document.getElementById("Nivel").value=Nivel;
}

